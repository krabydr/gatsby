module.exports = {
    siteMetadata: {
        title: 'KRABY',
        description: 'Site we built together'
    },
    plugins: ['gatsby-plugin-emotion','gatsby-plugin-sharp','gatsby-transformer-sharp', 'gatsby-plugin-react-helmet', {
        resolve: 'gatsby-mdx',
        options: {
            defaultLayouts: {
                default: require.resolve('./src/components/layout.js')
            },
            gatsbyRemarkPlugins:[
                {resolve:'gatsby-remark-images'}
            ]
        }
    },
        {
            resolve: 'gatsby-source-filesystem',
            options: {
                name: 'posts',
                path: 'posts',
            }
        },
        {
            resolve: `gatsby-plugin-sharp`,
            options: {
                useMozJpeg: true,
            },
        },
        {
            resolve: 'gatsby-source-filesystem',
            options: {
                name: 'images',
                path: 'images',
            }
        },
        {
            resolve: 'gatsby-source-instagram',
            options: {
                username: 'jkz_dr',
            }
        },
        {
            resolve: 'gatsby-plugin-webpack-bundle-analyzer',
            options: {
                production: true,
                disable:!process.env.ANALYZE_BUNDLE_SIZE,
                generateStatsFile:true,
                analyzerMode:'static'
            }
        },
        {
            resolve: `gatsby-source-behance-collection`,
            options: {
                // Visit a collection page and grab the number after collection
                // Ex.https://www.behance.net/collection/28447865/Typefaces
                collectionId: '122869523', // 28447865
                // You can get your API Key here: https://www.behance.net/dev/register
                apiKey: 'yOEjVeOZZg7Wi9bkoW5t37K9GpaAdRnV',
            }
        }
    ]
};