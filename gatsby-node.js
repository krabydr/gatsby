
exports.createPages = async ({actions, graphql, reporter}) => {
    const result = await graphql(`
        query{
            allMdx{
                nodes{
                    frontmatter{
                        slug
                    }
                }
            }
        }
    `);

    if (result.error) {
        reporter.panic('Failed to create posts', result.error);
    }

    const posts = result.data.allMdx.nodes;
    posts.forEach(post => {
        actions.createPage({
            path: post.frontmatter.slug,
            component: require.resolve('./src/template/post.js'),
            context: {
                slug: post.frontmatter.slug,
            },
        });
    });




    const result_b = await graphql(`
        query{
            allBehanceCollectionProjects{
                nodes{
                    id
                    name
                    url
                    areas
                    covers {
                        cover_original
                        }
                    }
                }
            }
    `);

    const posts_b = result_b.data.allBehanceCollectionProjects.nodes;

    posts_b.forEach(post => {
        actions.createPage({
            path: `/behance/${post.id}`,
            component: require.resolve('./src/template/behance_page.js'),
            context: {
                slug: post.id,
            },
        });
    });
};

