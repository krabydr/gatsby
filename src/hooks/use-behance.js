import {graphql, useStaticQuery} from 'gatsby';

const useBehance = () => {
    const data = useStaticQuery(graphql`
        query{
            behanceCollection{
                owners{
                  username
                  url
                }
             }
            allBehanceCollectionProjects{
                nodes{
                    id
                    name
                    url
                    areas
                    covers {
                        cover_original
                        }
                    }
                }
            }
    `);
    let dataPosts = data.allBehanceCollectionProjects.nodes.map(node => ({
        id: node.id,
        name: node.name,
        url: node.url,
        areas: node.areas,
        image: node.covers.cover_original,
    }));

    let postBehance = data.behanceCollection.owners.map(node => ({
        username: node.username,
        url: node.url,

    }));
    return [dataPosts, postBehance]

};
export default useBehance;