import React from 'react';
import {graphql} from "gatsby";
import {css} from "@emotion/core";
import Layout from '../components/layout';
import useWindow from "../hooks/use-window";



export const query = graphql`
    query{
            allBehanceCollectionProjects{
                nodes{
                    id
                    name
                    url
                    areas
                    modules {
                        src
                        }
                    }
                }
            }
`;


const BTemplate = ({data: {allBehanceCollectionProjects: {nodes: dataNew}},pathContext:{slug:slug}}) => {
    let n = slug.lastIndexOf('/');
    let result = slug.substring(n + 1);
    return (
        <Layout>
            <div css={css`display: flex;flex-wrap: wrap;`}>
                {dataNew.map(photo => {
                    if (photo.id === result) {
                        return (
                            <div>
                                <h1 key={photo.id} css={css`margin-bottom: 2rem;`}>{photo.name}</h1>
                                {
                                    photo.modules.map((item, i) => {
                                        return (
                                            <img
                                                key={i}
                                                src={item.src}
                                                alt={photo.caption}
                                                css={css`width: 100% ;{margin-top: 0;}`}
                                            />

                                        )
                                    })
                                }
                            </div>
                        )
                    }
                })}
            </div>
        </Layout>
    );
};

export default BTemplate;