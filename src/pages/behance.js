import React from 'react';
import {Link} from 'gatsby';
import Layout from "../components/layout";
import useBehance from "../hooks/use-behance";
import {css} from "@emotion/core";


export default () => {
    const behance = useBehance();
    return (

        <Layout>
            <h1>About my behance <a href={behance[1][0].url}>@{behance[1][0].username}</a> </h1>
            <br/>
            <Link to={'/'}>&larr; Back to home </Link>
            <div css={css`display: flex;flex-wrap: wrap;`}>
                {behance[0].map((photo, i) => (
                    <a
                        key={photo.id}
                        href={`/behance/${photo.id}`}
                        css={css`
                         position: relative;
                         box-shadow: 0;
                         display: block;
                         margin: 0.5rem;
                         max-width: calc(33% - 1rem);
                         width: 33%;
                         max-height: max-content;
                         transition: 200ms box-shadow linear;
                         
                          :focus,:hover{
                          box-shadow: 0 2px 14px #22222244;
                          z-index: 10;
                          .hovering{
                          position: absolute;
                          display: flex;
                          flex-direction: column;
                          justify-content: flex-end;
                          top: 0;
                          left: 0;
                          padding: 1rem;
                          width: 100%;
                           height: 100%;
                           visibility: visible;
                            background: rgba(102,51,153,0.33);
                            h4{
                            color: white;
                            }
                          }
                          }
                          .hovering{
                          visibility: hidden;
                          padding: 0;
                          width: 0;
                          height: 0;
                          margin: 0;
                          transition: background-color , color 3500ms ease ;
                          }
                        `}
                    >
                        <img
                            src={photo.image}
                            alt={photo.areas}
                            css={css`
                            width: 100%;
                            height: 100%;
                            *{
                            margin-top: 0;
                            margin-bottom: 0;
                            }
                        `}
                        />
                        <div className={"hovering"}>
                            <h4> {photo.name}</h4>
                        </div>
                    </a>
                ))}
            </div>
        </Layout>
    )
};