import React from 'react';
import {Global, css} from '@emotion/core';
import Helmet from 'react-helmet';
import Header from "./heaeder";
import useSiteMetadata from '../hooks/use-sitemetadata';

const Layout = ({children}) => {
    const {title, description} = useSiteMetadata();
    return (
        <>
            <Global
                styles={css`
                *{ 
                box-sizing:border-box;
                margin:0;
                }
                *+*{
                margin-top:1rem;
                }
                body,html{
                margin:0;
                color:#555;
                font-family:-apple-system,BlinkMacSystemFont , 'Segoe UI', Roboto ,helvetica , Arial, sans-serif;
                font-size:18px;
                line-height:1.4;
                >div{
                margin-top:0;
                }
                }
                
                h1,h2,h3,h4,h5,h6{
                color:#222;
                line-height:1.1;
                    + *{
                margin-top:0.5rem;
                }
                }
                strong{
                color:#222;
                }
                li{
                margin-top:0.25rem;
                }
                a{
                text-decoration: none;
                color: rebeccapurple;
                &:hover{
                color: orchid;
                }
                }
                `}/>
            <Helmet>
                <html lang={"en"}/>
                <title>{title}</title>
                <meta name={'description'} content={description}/>
            </Helmet>
            <Header/>
            <main
                css={css`
                margin:2rem auto;
                max-width:90vw;
                width:1167px;
                 @media (max-width: 768px) {
                   max-width:100vw;
                   width: calc(100% - 2rem);
                 }
            `}
            >{children}</main>
        </>
    )
}

export default Layout;