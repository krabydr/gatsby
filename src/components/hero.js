import React from 'react';
import styled from '@emotion/styled';
import {Link, graphql, useStaticQuery} from "gatsby";
import BackgroundImage from 'gatsby-background-image'

const ImageBackground = styled(BackgroundImage)`
  background-position: center center;
  background-size: cover;
  height: 50vh;
  +*{
  margin-top: 0;
  }
`;

const TextBox = styled('div')`
background-image: linear-gradient(to top , #ddbbffdd 2rem , #ddbbff00);
display: flex;
height: 100%;
justify-content: flex-end;
flex-direction: column;
padding: 0 calc((100vw - 1167px ) / 2) 2rem;

h1{
text-shadow: 1px 1px 3px #eeddff66;
font-size: 2.25rem;
}
p,a{
color: #222;
margin-top: 0;
}
a{
margin-top: 0.5rem;
}
`;

const Hero = () => {
    const {image} = useStaticQuery(graphql`
        query{
            image:file(relativePath:{eq:"lv.jpg"}){
                sharp:childImageSharp{
                  fluid{
                    ...GatsbyImageSharpFluid_withWebp
                  }
                }
             } 
        }
    `)
    return (
        <ImageBackground Tag={'section'} fluid={image.sharp.fluid}>
            <TextBox>
                <h1>My fetish site </h1>
                <p>
                    hello lviv <Link to={"/about"}> Learn about me &rarr;</Link>
                </p>
            </TextBox>
        </ImageBackground>
    );
};

export default Hero;