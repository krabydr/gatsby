import React from 'react';
import {css} from '@emotion/core';
import {Link} from 'gatsby';
import ReadLink from '../components/read-link'


const PostPreview = ({post}) => {
    console.log(post)
    return (
        <article
            css={css`
            display: flex;
           
            border-bottom: 1px solid #ddd;
            margin-top: 0;
            padding-bottom: 1rem;
            
            &:first-of-type{
            margin-top: 1rem;
            }
            `}
        >
            <div>
                <h3><Link to={post.slug}>{post.title}</Link></h3>
                <p>{post.excerpt}</p>
                <ReadLink to={post.slug}>read this post &rarr;</ReadLink>
            </div>
        </article>
    );
};

export default PostPreview;